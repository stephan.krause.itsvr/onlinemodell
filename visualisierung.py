# coding=utf-8
import math

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rcParams
from matplotlib.animation import FuncAnimation
from IPython.display import HTML

from sgltm import *


class TrafficGenerator(object):
    NAME = ""

    def generate(self):
        pass


class TrafficGeneratorFactory(object):
    _classes = {}

    @classmethod
    def register(cls, new_cls):
        cls._classes[new_cls.NAME] = new_cls
        return new_cls

    @classmethod
    def create_generator(cls, name):
        return cls._classes[name]()


@TrafficGeneratorFactory.register
class RandomizedHeavisideGenerator(TrafficGenerator):
    NAME = 'sprung'

    def generate(self, n, n_active, q, relstd):
        rs = np.random.RandomState()
        return [0.] + [min(max(rs.normal(q, q * relstd), 0.0), q * (1.0 + relstd)) for i in range(n_active)] + [0.0] * (
                    n - n_active - 1)


@TrafficGeneratorFactory.register
class RandomizedBellGenerator(TrafficGenerator):
    NAME = 'glocke'

    def mollifier(self, x):
        return math.e * math.exp(-1.0 / (1.0 - x ** 2)) if -1.0 < x and x < 1.0 else 0.0

    def bell(self, i, n):
        return self.mollifier((float(i) - float(n) / 2.0) / float(n) * 2.0)

    def generate(self, n, n_active, q, relstd):
        rs = np.random.RandomState()
        return [0.] + [min(max(rs.normal(q * self.bell(i, n_active), q * relstd * self.bell(i, n_active)), 1e-5), q) for
                       i in range(n_active)] + [0.0] * (n - n_active - 1)


@TrafficGeneratorFactory.register
class RandomizedSineGenerator(TrafficGenerator):
    NAME = 'sinus'

    def sinus(self, i, n, p):
        return (1.0 + math.sin(- math.pi / 2.0 + p * 2.0 * math.pi * float(i) / float(n))) / 2.0

    def generate(self, n, n_active, q, relstd, n_peaks=2):
        rs = np.random.RandomState()
        return [0.] + [min(
            max(rs.normal(q * self.sinus(i, n_active, n_peaks), q * relstd * self.sinus(i, n_active, n_peaks)), 1e-5),
            q) for i in range(n_active)] + [0.0] * (n - n_active - 1)


@TrafficGeneratorFactory.register
class RandomizedWaveGenerator(TrafficGenerator):
    NAME = 'welle'

    def sinus(self, i, n, p):
        return (1.0 + math.sin(- math.pi / 2.0 + p * 2.0 * math.pi * float(i) / float(n))) / 2.0

    def generate(self, n, n_active, q, relstd, n_peaks=2):
        rs = np.random.RandomState()
        return [0.] + [min(max(rs.normal(q * (1 + relstd * 0.5 * self.sinus(i, n_active, n_peaks)),
                                         q * relstd * 0.5 * self.sinus(i, n_active, n_peaks)), 1e-5), q * (1 + relstd))
                       for i in range(n_active)] + [0.0] * (n - n_active - 1)


def ampel(umlaufzeit, gruenzeitanteil, ueberlastung=1.0, kurvenform='sprung', schwankungsbreite=0.1):
    assert gruenzeitanteil > 0.0 and gruenzeitanteil < 1.0
    assert ueberlastung >= 1.0 and ueberlastung * gruenzeitanteil < 1.0
    assert schwankungsbreite >= 0.0

    func_params = SGLTMFuncParams(
        min_wave_speed=kph_to_mps(1.0),
        hypocritical_gamma=0.5,
        hypercritical_gamma=0.0
    )

    n_active = int(5 * umlaufzeit)
    n_links = int((float(n_active) * vph_to_vps(1800.0) * gruenzeitanteil * (ueberlastung - 1) * 6.0) / 100.0) + 5
    n_tau = int(math.ceil(float(n_links) * 100.0 / kph_to_mps(41.0) + ueberlastung * float(n_active) + umlaufzeit))
    T = float(n_tau)

    links = [
        SGLTMLink(
            id=id,
            length=100.0,
            capacity=vph_to_vps(1800.0),
            max_density=1 / 6.0,
            freeflow_speed=kph_to_mps(41.0),
            jam_wave_speed=kph_to_mps(25.0),
            func_params=func_params
        )
        for id in range(1, n_links+1)
    ]

    model = SimplifiedGLTM(
        links=links,
        T=T,
        n_tau=n_tau
    )

    generator = TrafficGeneratorFactory.create_generator(kurvenform)
    entry_flows = generator.generate(n=n_tau, n_active=n_active, q=vph_to_vps(1800.0) * gruenzeitanteil * ueberlastung,
                                     relstd=schwankungsbreite)

    model.simulate(
        entry_flows=entry_flows,
        exit_capacities=[
            vph_to_vps(1800.0) if i % int(umlaufzeit) >= (1 - gruenzeitanteil) * umlaufzeit else 0.0
            for i in range(n_tau)
        ]
    )

    return model


def engpass(ueberlastung=1.0, kurvenform='sprung', schwankungsbreite=0.1):
    assert schwankungsbreite >= 0.0
    assert ueberlastung >= 1.0 and ueberlastung < 1.0 + 1.0 / 3.0

    func_params = SGLTMFuncParams(
        min_wave_speed=kph_to_mps(1.0),
        hypocritical_gamma=0.5,
        hypercritical_gamma=0.0
    )

    n_active = 500
    n_links = 5 + int(math.ceil(float(n_active) * vph_to_vps(6900.0) * (ueberlastung-1) / 400.0 * 6.0)) + 4
    n_tau = int(math.ceil(float(n_links) * 100.0 / kph_to_mps(80.0) + n_active * ueberlastung + 100.0))
    T = float(n_tau)

    links = [
        SGLTMLink(
            id=id,
            length=100.0,
            capacity=vph_to_vps(6900.0 if id in (n_links - 3, n_links - 2) else 9200.0),
            max_density=3.0 / 6.0 if id in (n_links - 3, n_links - 2) else 4.0 / 6.0,
            freeflow_speed=kph_to_mps(80.0),
            jam_wave_speed=kph_to_mps(30.0),
            func_params=func_params
        )
        for id in range(1, n_links + 1)
    ]

    model = SimplifiedGLTM(
        links=links,
        T=T,
        n_tau=n_tau
    )


    generator = TrafficGeneratorFactory.create_generator(kurvenform)
    entry_flows = generator.generate(n=n_tau, n_active=n_active, q=vph_to_vps(6900.0) * ueberlastung,
                                     relstd=schwankungsbreite)

    model.simulate(
        entry_flows=entry_flows,
        exit_capacities=[vph_to_vps(9200.0)] * n_tau
    )

    return model


def chaos(ueberlastung=1.0, kurvenform='sprung', schwankungsbreite=0.1):
    assert schwankungsbreite >= 0.0
    assert ueberlastung >= 1.0 and ueberlastung < 2.0

    func_params = SGLTMFuncParams(
        min_wave_speed=kph_to_mps(1.0),
        hypocritical_gamma=0.5,
        hypercritical_gamma=0.0
    )

    n_active = 500
    n_links = 5 + int(math.ceil(float(n_active) * vph_to_vps(1800.0) * (ueberlastung - 1) / 200.0 * 6.0))
    n_tau = int(math.ceil(float(n_links) * 100.0 / kph_to_mps(80.0) + n_active * ueberlastung + 100.0))
    T = float(n_tau)

    links = [
        SGLTMLink(
            id=id,
            length=100.0,
            capacity=vph_to_vps(3600.0),
            max_density=4.0 / 6.0,
            freeflow_speed=kph_to_mps(80.0),
            jam_wave_speed=kph_to_mps(25.0),
            func_params=func_params
        )
        for id in range(1, n_links + 1)
    ]


    model = SimplifiedGLTM(
        links=links,
        T=T,
        n_tau=n_tau
    )

    generator = TrafficGeneratorFactory.create_generator(kurvenform)
    entry_flows = generator.generate(n=n_tau, n_active=n_active, q=vph_to_vps(1800.0) * ueberlastung,
                                     relstd=schwankungsbreite)

    chaos_generator = TrafficGeneratorFactory.create_generator('sprung')
    exit_capacities = chaos_generator.generate(n=n_tau, n_active=n_tau, q=vph_to_vps(1800.0), relstd=0.5)

    model.simulate(
        entry_flows=entry_flows,
        exit_capacities=exit_capacities
    )

    return model

def animiere(frame, model, cm, inv_cm, v_norm, q_norm, N_norm, speed_bars, n_bars, entry_flow_plot, exit_capacity_plot):
    for l, b in enumerate(speed_bars):
        v = mps_to_kph(model.link_states[frame][l].speed)
        b.set_height(v, )
        b.set_color(cm(v_norm(v)))
    for l, b in enumerate(n_bars):
        N = model.link_states[frame][l].N
        b.set_height(N)
        b.set_color(inv_cm(N_norm(N)))
    entry_flow_plot.set_data(range(frame), [vps_to_vph(model.entry_node_states[i].flow) for i in range(frame)])
    entry_flow_plot.set_color(inv_cm(q_norm(vps_to_vph(model.entry_node_states[frame].flow))))
    exit_capacity_plot.set_data(range(frame), [vps_to_vph(model.exit_node_states[i].capacity) for i in range(frame)])
    exit_capacity_plot.set_color(cm(q_norm(vps_to_vph(model.exit_node_states[frame].capacity))))

def zeige(model):
    rcParams['animation.embed_limit'] = 100.0

    v_max = max([mps_to_kph(link.freeflow_speed) for link in model.links])
    N_max = max([link.max_density * link.length for link in model.links])
    q_max = max([vps_to_vph(link.capacity) for link in model.links])

    v_norm = plt.Normalize(0.0, v_max)
    N_norm = plt.Normalize(0.0, N_max)
    q_norm = plt.Normalize(0.0, q_max)
    cm = plt.get_cmap('RdYlGn')
    inv_cm = plt.get_cmap('RdYlGn').reversed()

    ls = np.array([l.length for l in model.links])
    L = ls.sum()
    xs = ls.cumsum() - ls * 0.5
    ws = ls * 0.75

    fig, axes = plt.subplots(2, 2, sharex='col', figsize=(16.0, 12.0))

    speed_bars = axes[0, 0].bar(xs, [v_max * 1.05] * len(model.links), align='center', width=ws)
    axes[0, 0].set_title('Geschwindigkeit [km/h]')
    axes[0, 0].set_xlim(0, L)

    n_bars = axes[1, 0].bar(xs, [N_max * 1.05] * len(model.links), align='center', width=ws)
    axes[1, 0].set_title('Anzahl Fahrzeuge auf dem Link')
    axes[1, 0].set_xlim(0, L)
    axes[1, 0].set_xlabel(u'Länge [m]')

    entry_flow_plot, = axes[0, 1].plot([], [])
    axes[0, 1].set_xlim(0, model.n_tau)
    axes[0, 1].set_ylim(0, q_max * 1.05)
    axes[0, 1].set_title(u'Verkehrsstärke am Eingang [Fzg/h]')

    exit_capacity_plot, = axes[1, 1].plot([], [])
    axes[1, 1].set_xlim(0, model.n_tau)
    axes[1, 1].set_ylim(0, q_max * 1.05)
    axes[1, 1].set_xlabel(u'Zeit [s]')
    axes[1, 1].set_title(u'Ausgangskapazität [Fzg/h]')

    animation = FuncAnimation(
        fig=fig,
        func=animiere,
        frames=model.n_tau,
        fargs=(model, cm, inv_cm, v_norm, q_norm, N_norm, speed_bars, n_bars, entry_flow_plot, exit_capacity_plot),
        blit=False,
        repeat=False,
        interval=200
    )

    html = HTML(animation.to_jshtml())

    _fig = fig.clear()

    return html

def kurve(kurvenform, schwankungsbreite):
    generator = TrafficGeneratorFactory.create_generator(kurvenform)
    flows = generator.generate(n=1000, q=1000.0, n_active=500, relstd=schwankungsbreite)

    plt.plot(range(len(flows)), flows)


