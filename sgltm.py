
def mps_to_kph(v):
    return v / 1000.0 * 3600.0

def kph_to_mps(v):
    return v * 1000.0 / 3600.0

def vph_to_vps(q):
    return q / 3600.0

def vps_to_vph(q):
    return q * 3600.0

class SimplifiedGLTM(object):
    def __init__(self, links, T, n_tau):
        self.links = links
        self.nodes = [SGLTMNode(id=i+2, in_link=links[i], out_link=links[i + 1]) for i in range(len(links) - 1)]
        self.entry_node = SGLTMNode(id=1, in_link=None, out_link=links[0])
        self.exit_node = SGLTMNode(id=len(links)+1, in_link=links[-1], out_link=None)

        self.T = T
        self.tau = float(T) / float(n_tau)
        self.n_tau = n_tau

        self.instants = [float(i) * self.tau for i in range(2 * n_tau)]

        self.link_states = [
            [
                SGLTMLinkState(link, instant) for link in self.links
            ]
            for instant in self.instants
        ]

        self.node_states = [
            [
                SGLTMNodeState(node, instant) for node in self.nodes
            ]
            for instant in self.instants
        ]

        self.entry_node_states = [
            SGLTMNodeState(self.entry_node, instant) for instant in self.instants
        ]

        self.exit_node_states = [
            SGLTMNodeState(self.exit_node, instant) for instant in self.instants
        ]

    def simulate(self, entry_flows, exit_capacities):
        for i in range(self.n_tau):
            self.entry_node_states[i].flow = entry_flows[i]
            self.exit_node_states[i].capacity = exit_capacities[i]

        self.initialize_link_states()

        for i in range(1, self.n_tau):
            self.compute_sending_flows(i)
            self.compute_receiving_flows(i)
            self.compute_node_flows(i)
            self.forward_propagation(i)
            self.backward_propagation(i)
            self.compute_flows(i)

        self.compute_exit_times()

    def initialize_link_states(self):
        self.link_states[0][0].initialize(f=self.entry_node_states[0].flow)

        for link_state in self.link_states[0][1:]:
            link_state.initialize()

        for i in range(len(self.links)):
            # initialize H
            F0 = self.link_states[0][i].F
            f0 = self.link_states[0][i].f
            u = self.links[i].length / self.links[i].hypocritical_wave_speed(f0)
            j = 0
            while float(j) * self.tau <= u:
                self.link_states[j][i].init_H(F0, f0, u - float(j) * self.tau)
                j += 1

            # initialize G
            E0 = self.link_states[0][i].E
            e0 = self.link_states[0][i].e
            z = - self.links[i].length / self.links[i].hypercritical_wave_speed(e0)
            h = 0
            while float(h) * self.tau <= z:
                self.link_states[h][i].init_G(E0, e0, z - float(h) * self.tau)
                h += 1

            self.link_states[0][i].u = u
            self.link_states[0][i].j = j
            self.link_states[0][i].z = z
            self.link_states[0][i].h = h

    def compute_sending_flows(self, i):
        for l in range(len(self.links)):
            self.link_states[i][l].compute_sending_flow(self.link_states[i - 1][l], self.tau)

    def compute_receiving_flows(self, i):
        for l in range(len(self.links)):
            self.link_states[i][l].compute_receiving_flow(self.link_states[i - 1][l], self.tau)

    def compute_node_flows(self, i):
        self.entry_node_states[i].compute_node_capacity(None, self.link_states[i][0])
        node_flow = self.entry_node_states[i].compute_node_flow(None, self.link_states[i][0])
        self.link_states[i][0].f = node_flow

        for n in range(len(self.nodes)):
            self.node_states[i][n].compute_node_capacity(self.link_states[i][n], self.link_states[i][n + 1])
            node_flow = self.node_states[i][n].compute_node_flow(self.link_states[i][n], self.link_states[i][n + 1])
            self.link_states[i][n].e = node_flow
            self.link_states[i][n + 1].f = node_flow

        self.exit_node_states[i].compute_node_capacity(self.link_states[i][-1], None)
        node_flow = self.exit_node_states[i].compute_node_flow(self.link_states[i][-1], None)
        self.link_states[i][-1].e = node_flow

    def forward_propagation(self, i):
        for l in range(len(self.links)):
            link = self.links[l]

            t0 = self.link_states[i - 1][l].u
            j = self.link_states[i - 1][l].j
            Q0 = self.link_states[i - 1][l].H_max

            t1 = (i - 1) * self.tau + link.length / link.hypocritical_wave_speed(self.link_states[i][l].f)
            t2 = t1 + self.tau

            Q1 = self.link_states[i - 1][l].F + link.length * link.hypocritical_density_delta(self.link_states[i][l].f)
            Q2 = Q1 + self.link_states[i][l].f * self.tau

            if t1 >= t0:  # correction to paper (there seems to be an error)
                while j * self.tau < t1 and j < len(self.link_states):
                    # the formula is adapted to account for numerical instabilities when t1 is approximately the same
                    # as t0 (a very common case); the value must be between Q0 and Q1
                    Q = max(min(Q0 + (j * self.tau - t0) * (Q1 - Q0) / (t1 - t0), max(Q0, Q1)), min(Q0, Q1))
                    if self.link_states[j][l].H is None or self.link_states[j][l].H > Q:
                        self.link_states[j][l].H = Q
                    j += 1
            else:
                j -= 1
                while j * self.tau > t1:
                    # see above
                    Q = max(min(Q0 + (j * self.tau - t0) * (Q1 - Q0) / (t1 - t0), max(Q0, Q1)), min(Q0, Q1))
                    if self.link_states[j][l].H is None or self.link_states[j][l].H > Q:
                        self.link_states[j][l].H = Q
                    j -= 1
                    assert j > i
                j += 1

            while j * self.tau < t2 and j < len(self.link_states):
                # see above
                Q = max(min(Q1 + (j * self.tau - t1) * (Q2 - Q1) / (t2 - t1), max(Q1, Q2)), min(Q1, Q2))
                if self.link_states[j][l].H is None or self.link_states[j][l].H > Q:
                    self.link_states[j][l].H = Q
                j += 1

            self.link_states[i][l].u = t2
            self.link_states[i][l].j = j
            self.link_states[i][l].H_max = Q2

    def backward_propagation(self, i):
        for l in range(len(self.links)):
            link = self.links[l]

            j = self.link_states[i - 1][l].h
            t0 = self.link_states[i - 1][l].z
            Q0 = self.link_states[i - 1][l].G_max

            t1 = (i - 1) * self.tau - link.length / link.hypercritical_wave_speed(self.link_states[i][l].e)
            t2 = t1 + self.tau

            Q1 = self.link_states[i - 1][l].E + \
                 link.length * link.max_density + \
                 link.length * link.hypercritical_density_delta(self.link_states[i][l].e)
            Q2 = Q1 + self.tau * self.link_states[i][l].e

            if t1 >= t0:  # correction to paper (there seems to be an error)
                while j * self.tau < t1 and j < len(self.link_states):
                    # see above
                    Q = max(min(Q0 + (j * self.tau - t0) * (Q1 - Q0) / (t1 - t0), max(Q0, Q1)), min(Q0, Q1))
                    if self.link_states[j][l].G is None or self.link_states[j][l].G > Q:
                        self.link_states[j][l].G = Q
                    j += 1
            else:
                j -= 1
                while j * self.tau > t1:
                    # see above
                    Q = max(min(Q0 + (j * self.tau - t0) * (Q1 - Q0) / (t1 - t0), max(Q0, Q1)), min(Q0, Q1))
                    if self.link_states[j][l].G is None or self.link_states[j][l].G > Q:
                        self.link_states[j][l].G = Q
                    j -= 1
                    assert j > i
                j += 1

            while j * self.tau < t2 and j < len(self.link_states):
                # see above
                Q = max(min(Q1 + (j * self.tau - t1) * (Q2 - Q1) / (t2 - t1), max(Q1, Q2)), min(Q1, Q2))
                if self.link_states[j][l].G is None or self.link_states[j][l].G > Q:
                    self.link_states[j][l].G = Q
                j += 1

            self.link_states[i][l].z = t2
            self.link_states[i][l].h = j
            self.link_states[i][l].G_max = Q2

    def compute_flows(self, i):
        for l in range(len(self.links)):
            self.link_states[i][l].compute_flows(self.link_states[i - 1][l], self.tau)

    def compute_exit_times(self):
        for l in range(len(self.links)):
            link = self.links[l]

            self.link_states[0][l].init_exit_time()
            j = 1

            for i in range(1, self.n_tau):
                while j < len(self.link_states) and self.link_states[j][l].E < self.link_states[i][l].F:
                    j += 1

                if self.link_states[i][l].F == self.link_states[i-1][l].F:
                    self.link_states[i][l].t = max(self.link_states[i-1][l].t, i * self.tau + link.length / link.freeflow_speed)
                elif j < len(self.link_states):
                    self.link_states[i][l].t = (j - 1) * self.tau +\
                                               (self.link_states[i][l].F - self.link_states[j-1][l].E) * self.tau /\
                                               (self.link_states[j][l].E - self.link_states[j-1][l].E)
                else:
                    self.link_states[i][l].t = self.tau * len(self.link_states)


class SGLTMFuncParams(object):
    def __init__(self, min_wave_speed, hypocritical_gamma, hypercritical_gamma):
        self.min_wave_speed = min_wave_speed
        self.hypocritical_gamma = hypocritical_gamma
        self.hypercritical_gamma = hypercritical_gamma

class SGLTMLink(object):
    def __init__(self, id, length, capacity, max_density, freeflow_speed, jam_wave_speed, func_params):
        self.id = id
        self.length = length
        self.capacity = capacity
        self.max_density = max_density
        self.freeflow_speed = freeflow_speed
        self.jam_wave_speed = jam_wave_speed
        self.func_params = func_params

        # calculate flow restraints for min_wave_speed
        if func_params.hypocritical_gamma > 0.0:
            q_max_hypo = capacity / self.hypocritical_nu * (1.0 - (func_params.min_wave_speed / freeflow_speed) ** (1.0 / func_params.hypocritical_gamma))
        else:
            q_max_hypo = capacity

        if func_params.hypercritical_gamma > 0.0:
            q_max_hyper = capacity / self.hypercritical_nu * (1.0 - (func_params.min_wave_speed / jam_wave_speed) ** (1.0 / func_params.hypercritical_gamma))
        else:
            q_max_hyper = capacity

        self.max_hypocritical_flow = min(q_max_hypo, q_max_hyper)
        self.max_hypercritical_flow = min(q_max_hypo, q_max_hyper)

    @property
    def hypocritical_nu(self):
        return 1 - (self.func_params.min_wave_speed / self.freeflow_speed) ** (1 / self.func_params.hypocritical_gamma)

    @property
    def hypercritical_nu(self):
        return 1 - (self.func_params.min_wave_speed / self.jam_wave_speed) ** (1 / self.func_params.hypercritical_gamma)

    def hypocritical_speed(self, f):
        k = self.hypocritical_density(f)
        return f / k if k > 0.0 else self.freeflow_speed

    def hypercritical_speed(self, e):
        return e / self.hypercritical_density(e)

    def hypocritical_wave_speed(self, f):
        if self.func_params.hypocritical_gamma > 0.0:
            return max(
                self.freeflow_speed * (1 - self.hypocritical_nu * f / self.capacity) ** self.func_params.hypocritical_gamma,
                self.func_params.min_wave_speed
            )
        else:
            return self.freeflow_speed

    def hypercritical_wave_speed(self, e):
        if self.func_params.hypercritical_gamma > 0.0:
            return -max(
                self.jam_wave_speed * (1 - self.hypercritical_nu * e / self.capacity) ** self.func_params.hypercritical_gamma,
                self.func_params.min_wave_speed
            )
        else:
            return -self.jam_wave_speed

    def hypocritical_density(self, f):
        q = min(f, self.max_hypocritical_flow)
        if self.func_params.hypocritical_gamma > 0.0:
            return self.capacity / \
                   self.freeflow_speed / \
                   self.hypocritical_nu / \
                   (1.0 - self.func_params.hypocritical_gamma) * \
                   (1.0 -
                        (1.0 - self.hypocritical_nu * q / self.capacity) ** (1.0 - self.func_params.hypocritical_gamma)
                   )
        else:
            return q / self.freeflow_speed

    def hypercritical_density(self, e):
        q = min(e, self.max_hypercritical_flow)
        if self.func_params.hypercritical_gamma > 0.0:
           return self.max_density - \
                   self.capacity / \
                   self.jam_wave_speed / \
                   self.hypercritical_nu / \
                   (1.0 - self.func_params.hypercritical_gamma) * \
                   (1.0 -
                        (1.0 - self.hypercritical_nu * q / self.capacity) ** (1.0 - self.func_params.hypercritical_gamma)
                   )
        else:
            return self.max_density - q / self.jam_wave_speed


    def hypocritical_density_delta(self, f):
        k = f / self.hypocritical_wave_speed(f)
        return max(k - self.hypocritical_density(f), 0.0)

    def hypercritical_density_delta(self, e):
        k = -e / self.hypercritical_wave_speed(e)
        return min(self.hypercritical_density(e) + k - self.max_density, 0.0)


class SGLTMNode(object):
    def __init__(self, id, in_link, out_link):
        self.id = id
        self.in_link = in_link
        self.out_link = out_link


class SGLTMLinkState(object):
    def __init__(self, link, instant):
        self.link = link
        self.instant = instant

        # state variables
        self.f = 0.0  # entry flow
        self.e = 0.0  # exit flow
        self.F = 0.0  # cumulative entry flow
        self.E = 0.0  # cumulative exit flow
        self.S = 0.0  # vertical queue
        self.R = 0.0  # storage capacity
        self.N = 0.0  # number of vehicles
        self.H = None  # potential cumulative exit flow including the queued vehicles
        self.G = None  # potential cumulative entry flow including the storage capacity
        self.r = 0.0  # receiving flow
        self.s = 0.0  # sending flow

        # variables for capacity reduction
        self.eta = 1.0
        self.mu = 1.0

        # variables for flow propagation
        self.u = 0.0
        self.j = 0.0
        self.H_max = 0.0
        self.z = 0.0
        self.h = 0.0
        self.G_max = 0.0

        # exit time
        self.t = 0.0

    @property
    def entry_flow(self):
        return self.f

    @property
    def exit_flow(self):
        return self.e

    @property
    def sending_flow(self):
        return self.s

    @property
    def receiving_flow(self):
        return self.r

    @property
    def entry_capacity(self):
        return self.mu * self.link.capacity

    @property
    def exit_capacity(self):
        return self.eta * self.link.capacity

    @property
    def vehicles_in_queue(self):
        return self.S

    @property
    def queue_length(self):
        return self.S / self.link.hypercritical_density(self.e)

    @property
    def storage_capacity(self):
        return self.R

    @property
    def saturation(self):
        return 1.0 - self.R / self.link.length / self.link.max_density

    @property
    def vehicles_on_link(self):
        return self.N

    @property
    def exit_time(self):
        return self.t

    @property
    def travel_time(self):
        return self.t - self.instant

    @property
    def speed(self):
        return self.link.length / self.travel_time

    @property
    def density(self):
        return self.N / self.link.length

    def initialize(self, f=0.0, e=0.0, S=0.0):
        self.f = f
        self.e = e
        self.S = S

        self.E = - self.link.length * self.f * self.link.hypocritical_speed(self.f)

    def init_G(self, E0, e0, delta_t):
        self.G_max = E0 + self.link.length * self.link.max_density + \
                     self.link.length * self.link.hypercritical_density_delta(e0)
        self.G = self.G_max - delta_t * e0

    def init_H(self, F0, f0, delta_t):
        self.H_max = F0 + self.link.length * self.link.hypocritical_density_delta(f0)
        self.H = self.H_max - f0 * delta_t

    def init_exit_time(self):
        self.t = self.link.length / self.link.freeflow_speed

    def compute_sending_flow(self, previous_state, tau):
        if self.H is None:
            self.s = 0.0 # ???
        else:
            self.s = max(
                0.0,
                min(
                    (self.H - previous_state.E) / tau,
                    self.link.capacity * self.eta
                )
            )

    def compute_receiving_flow(self, previous_state, tau):
        if self.G is None:
            self.r = self.link.capacity * self.mu
        else:
            self.r = max(
                0.0,
                min(
                    (self.G - previous_state.F) / tau,
                    self.link.capacity * self.mu
                )
            )


    def compute_flows(self, previous_state, tau):
        self.F = previous_state.F + tau * self.f
        self.E = previous_state.E + tau * self.e
        self.S = self.H - self.E
        self.R = self.G - self.F
        self.N = self.F - self.E


class SGLTMNodeState(object):
    def __init__(self, node, instant):
        self.node = node

        self.capacity = 0.0  # in link exit capacity == out link entry capacity
        self.flow = 0.0  # in link exit flow == out link entry flow

    def compute_node_capacity(self, in_link_state, out_link_state):
        if in_link_state is None:
            self.capacity = out_link_state.entry_capacity
        elif out_link_state is None:
            self.capacity = min(self.capacity, in_link_state.exit_capacity)
        else:
            self.capacity = min(
                in_link_state.exit_capacity,
                out_link_state.entry_capacity
            )

    def compute_node_flow(self, in_link_state, out_link_state):
        if in_link_state is None:
            self.flow = min(
                self.flow,
                out_link_state.receiving_flow
            )
        elif out_link_state is None:
            self.flow = min(
                self.capacity,
                in_link_state.sending_flow
            )
        else:
            self.flow = min(
                self.capacity,
                in_link_state.sending_flow,
                out_link_state.receiving_flow
            )

        return self.flow
